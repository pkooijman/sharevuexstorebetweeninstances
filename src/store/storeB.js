export default {
  namespaced: true,
  state: {
    msg: 'Hello from project B',
  },
  getters: {
    msg: state => state.msg,
    msgfromA: (state, getters, rootState, rootGetters) => rootGetters['proja/msg'],
  },
  mutations: {
    set: (state, payload) => {
      state.msg = payload
    }
  }
};
