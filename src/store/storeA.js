import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    proja: {
      namespaced: true,
      state: {
        msg: 'Hello from project A',
      },
      getters: {
        msg: state => state.msg,
        msgfromB: (state, getters, rootState, rootGetters) => rootGetters['projb/msg'],
      },
      mutations: {
        set: (state, payload) => { state.msg = payload}
      }
    }
  }
});

window.vuexStore = store;

export const storeA = store;

