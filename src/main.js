import Vue from 'vue';
import AppA from './Appa.vue';
import AppB from './Appb.vue';
import { storeA } from './store/storeA';
import { storeB } from './store/storeB';

Vue.config.productionTip = false;

new Vue({
  store: storeA,
  render: h => h(AppA)
}).$mount('#appa');


new Vue({
  render: h => h(AppB)
}).$mount('#appb');


